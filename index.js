import 'bootstrap/dist/css/bootstrap.css';
// import 'bootstrap/dist/css/bootstrap-theme.css';
import 'font-awesome/css/font-awesome.css'
import './style/custom.css';

import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Provider }  from 'react-redux';
import { AppContainer } from 'react-hot-loader';
import { BrowserRouter } from 'react-router';

import Root from 'pages/Root';
import store from 'store';

window.store = store;

const rootEl = document.getElementById('app');

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Root />
    </BrowserRouter>
  </Provider>,
  rootEl
);

if (module.hot) {
  module.hot.accept('./pages/Root', () => {
    const NextRoot = require('./pages/Root').default;
    ReactDOM.render(
      <Provider store={store}>
        <BrowserRouter>
          <NextRoot />
        </BrowserRouter>
      </Provider>,
      rootEl
    );
  });
}
