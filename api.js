import autobind from 'class-autobind'
import _ from 'lodash';
import axios from 'axios';
// import localStorage from './utils/localStorage';

function getToken() {
  return localStorage.getItem('jwt');
}

function setToken(token) {
  localStorage.setItem('jwt', token)
}

function removeToken(token) {
  localStorage.removeItem('jwt');
}

function hasToken() {
  return getToken() != null;
}

function getHeaders() {
  const result = {
    'Content-Type': 'application/json',
  };

  if (hasToken()) {
    result['Authorization'] = getToken();
  }

  return result;
}

export class Api {
  constructor(baseURL) {
    autobind(this);
    this.baseURL = baseURL;
  }

  // async checkAuth() {
  //   if (!hasToken()) {
  //     return null;
  //   }
  //
  //   try {
  //     // const res = await this.client().get('/api/users/me');
  //     const json = await this.get('/api/users/me');
  //
  //     return _.get(json, 'data', {});
  //   } catch (e) {
  //     throw e.response.statusText;
  //   }
  // }

  isGuest() {
    return !hasToken();
  }

  async signInWithEmailAndPassword(email, password) {
    try {
      const res = await axios.post('/api/tokens', {
        user: {
          email,
          password,
        }
      }, {
        baseURL: this.baseURL,
      })

      setToken(res.data.data.jwt);

      return res.data.data.user;
    } catch (error) {
      throw error.response.data;
    }
  }

  signOut() {
    removeToken();
  }

  async get(url) {
    try {
      const res = await axios.get(url, {
        baseURL: this.baseURL,
        headers: getHeaders(),
      })

      return res.data;
    } catch (error) {
      throw error.response.data;
    }
  }

  async post(url, data) {
    try {
      const res = await axios.post(url, data, {
        baseURL: this.baseURL,
        headers: getHeaders(),
      })

      return res.data;
    } catch (error) {
      throw error.response.data;
    }
  }

  async put(url, data) {
    try {
      const res = await axios.put(url, data, {
        baseURL: this.baseURL,
        headers: getHeaders(),
      })

      return res.data;
    } catch (error) {
      throw error.response.data;
    }
  }

  async patch(url, data) {
    try {
      const res = await axios.patch(url, data, {
        baseURL: this.baseURL,
        headers: getHeaders(),
      })

      return res.data;
    } catch (error) {
      throw error.response.data;
    }
  }

  async delete(url) {
    try {
      const res = await axios.delete(url, {
        baseURL: this.baseURL,
        headers: getHeaders(),
      })

      return res.data;
    } catch (error) {
      throw error.response.data;
    }
  }
}

const api = new Api(window.BACKEND_URL || window.location.origin);
window.api = api;
export default api;
