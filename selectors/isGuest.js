export default function isGuest(state) {
  return !state.auth.jwt;
}
