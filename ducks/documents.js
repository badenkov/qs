import { handleActions } from 'redux-actions';
import { filter, map } from 'lodash';
import { LOADING } from '../utils/loading';

export const FETCH_DOCUMENTS_REQUEST = 'app/documents/FETCH_DOCUMENTS_REQUEST';
export const FETCH_DOCUMENTS_SUCCESS = 'app/documents/FETCH_DOCUMENTS_SUCCESS';
export const FETCH_DOCUMENTS_FAILURE = 'app/documents/FETCH_DOCUMENTS_FAILURE';

export const CREATE_DOCUMENT_REQUEST = 'app/documents/CREATE_DOCUMENT_REQUEST';
export const CREATE_DOCUMENT_SUCCESS = 'app/documents/CREATE_DOCUMENT_SUCCESS';
export const CREATE_DOCUMENT_FAILURE = 'app/documents/CREATE_DOCUMENT_FAILURE';

export const UPDATE_DOCUMENT_REQUEST = 'app/documents/UPDATE_DOCUMENT_REQUEST';
export const UPDATE_DOCUMENT_SUCCESS = 'app/documents/UPDATE_DOCUMENT_SUCCESS';
export const UPDATE_DOCUMENT_FAILURE = 'app/documents/UPDATE_DOCUMENT_FAILURE';

export const UPLOAD_DOCUMENT_REQUEST = 'app/documents/UPLOAD_DOCUMENT_REQUEST';

export const DESTROY_DOCUMENT_REQUEST = 'app/documents/DESTROY_DOCUMENT_REQUEST';
export const DESTROY_DOCUMENT_SUCCESS = 'app/documents/DESTROY_DOCUMENT_SUCCESS';
export const DESTROY_DOCUMENT_FAILURE = 'app/documents/DESTROY_DOCUMENT_FAILURE';

export const fetchDocuments = (applicationID) => ({
  type: FETCH_DOCUMENTS_REQUEST,
  payload: { applicationID },
});

export const fetchDocumentsSuccess = (documents) => ({
  type: FETCH_DOCUMENTS_SUCCESS,
  payload: { documents },
});

export const fetchDocumentsFailure = (error) => ({
  type: FETCH_DOCUMENTS_FAILURE,
  payload: { error },
});

export const createDocument = (applicationID, document, onSuccess, onFailure) => ({
  type: CREATE_DOCUMENT_REQUEST,
  payload: { applicationID, document, onSuccess, onFailure },
});

export const createDocumentSuccess = (document) => ({
  type: CREATE_DOCUMENT_SUCCESS,
  payload: { document },
});

export const createDocumentFailure = (error) => ({
  type: CREATE_DOCUMENT_FAILURE,
  payload: { error },
});

export const updateDocument = (applicationID, id, document, onSuccess) => ({
  type: UPDATE_DOCUMENT_REQUEST,
  payload: {
    applicationID,
    id,
    document,
    onSuccess,
  },
});

export const updateDocumentSuccess = (document) => ({
  type: UPDATE_DOCUMENT_SUCCESS,
  payload: { document },
});

export const updateDocumentFailure = (error) => ({
  type: UPDATE_DOCUMENT_FAILURE,
  payload: { error },
});

export const uploadDocument = (applicationID, id, file, onSuccess) => ({
  type: UPLOAD_DOCUMENT_REQUEST,
  payload: {
    applicationID,
    id,
    file,
    onSuccess,
  },
});

export const destroyDocument = (applicationID, id, onSuccess) => ({
  type: DESTROY_DOCUMENT_REQUEST,
  payload: {
    applicationID,
    id,
    onSuccess,
  },
});

export const destroyDocumentSuccess = (id) => ({
  type: DESTROY_DOCUMENT_SUCCESS,
  payload: {
    id,
  },
});

export const destroyDocumentFailure = () => ({
  type: DESTROY_DOCUMENT_FAILURE,
});

const initialState = {
  items: [],
};

export default handleActions({
  [FETCH_DOCUMENTS_REQUEST]: (state) => ({
    ...state,
    items: LOADING,
  }),
  [FETCH_DOCUMENTS_SUCCESS]: (state, { payload: { documents } }) => ({
    ...state,
    items: documents,
  }),
  [FETCH_DOCUMENTS_FAILURE]: (state, { payload: { error } }) => ({
    ...state,
    error,
  }),

  [UPDATE_DOCUMENT_SUCCESS]: (state, { payload: { document } }) => ({
    ...state,
    items: map(state.items, (item) => item.id === document.id ? document : item),
  }),

  [DESTROY_DOCUMENT_SUCCESS]: (state, { payload: { id } }) => ({
    ...state,
    items: filter(state.items, (item) => item.id !== id),
  }),
}, initialState);
