import { handleActions, createAction } from 'redux-actions';

export const CLEAR = 'app/applications/CLEAR';

export const FETCH_REQUEST = 'app/applications/FETCH_REQUEST';
export const FETCH_SUCCESS = 'app/applications/FETCH_SUCCESS';
export const FETCH_FAILURE = 'app/applications/FETCH_FAILURE';

export const CREATE_REQUEST = 'app/applications/CREATE_REQUEST';
export const CREATE_SUCCESS = 'app/applications/CREATE_SUCCESS';
export const CREATE_FAILURE = 'app/applications/CREATE_FAILURE';

export const REMOVE_REQUEST = 'app/applications/REMOVE_REQUEST';
export const REMOVE_SUCCESS = 'app/applications/REMOVE_SUCCESS';
export const REMOVE_FAILURE = 'app/applications/REMOVE_FAILURE';

export const fetchApplications = createAction(FETCH_REQUEST) ;
export const fetchApplicationsSuccess = createAction(FETCH_SUCCESS, applications => applications) ;
export const fetchApplicationsFailure = createAction(FETCH_FAILURE, error => error) ;

export const createApplication = createAction(CREATE_REQUEST, (application, onSuccess) => ({ application, onSuccess }));
export const createApplicationSuccess = createAction(CREATE_SUCCESS, application => application);
export const createApplicationFailure = createAction(CREATE_FAILURE, error => error);

export const removeApplication = createAction(REMOVE_REQUEST, (id, onSuccess) => ({ id, onSuccess }));
export const removeApplicationSuccess = createAction(REMOVE_SUCCESS);
export const removeApplicationFailure = createAction(REMOVE_FAILURE);

const initialState = {
  items: [],
  count: 0,
  isFetching: true,
}

export default handleActions({
  [CLEAR]: state => initialState,

  [FETCH_REQUEST]: (state) => ({
    ...state,
    items: null,
    isFetching: true,
  }),
  [FETCH_SUCCESS]: (state, { payload }) => ({
      ...state,
      isFetching: false,
      items: payload,
  }),
  [FETCH_FAILURE]: (state, action) => ({
    ...state,
    isFething: false,
  }),

}, initialState);
