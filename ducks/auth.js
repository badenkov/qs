import { handleActions, createAction } from 'redux-actions';

// constants
export const SIGN_IN = 'app/auth/SIGN_IN';
export const SIGN_IN_SUCCESS = 'app/auth/SIGN_IN_SUCCESS';
export const SIGN_IN_FAILURE = 'app/auth/SIGN_IN_FAILURE';

export const SIGN_OUT = 'app/auth/SIGN_OUT';
export const SIGN_OUT_SUCCESS = 'app/auth/SIGN_OUT_SUCCESS';

export const CHECK_AUTH_SUCCESS = 'app/auth/CHECK_AUTH_SUCCESS';
export const CHECK_AUTH_FAILURE = 'app/auth/CHECK_AUTH_FAILURE';

// actions
export const signOut = createAction(SIGN_OUT);
export const signOutSuccess = createAction(SIGN_OUT_SUCCESS);

export const signInWithEmailAndPassword = createAction(SIGN_IN, (email, password ) => ({ email, password }));
export const signInSuccess = createAction(SIGN_IN_SUCCESS, user => user);
export const signInFailure = createAction(SIGN_IN_FAILURE, error => error);

export const checkAuthSuccess = createAction(CHECK_AUTH_SUCCESS, user => user);
export const checkAuthFailure = createAction(CHECK_AUTH_FAILURE, error => error);
3
// reducer
const initialState = {
  initialized: false,
  currentUser: null,
  signInValues: {},
  isSigningIn: false,
}

export default handleActions({
  [CHECK_AUTH_SUCCESS]: (state, { payload: currentUser }) => ({
    ...state,
    initialized: true,
    currentUser,
  }),
  [CHECK_AUTH_FAILURE]: (state) => ({
    ...state,
    initialized: true,
    currentUser: null,
  }),

  [SIGN_OUT_SUCCESS]: (state) => ({
    ...state,
    currentUser: null,
    error: null,
  }),

  [SIGN_IN]: (state, { payload: { email, password } }) => ({
    ...state,
    signInValues: { email, password },
    isSigningIn: true,
    error: null,
  }),
  [SIGN_IN_SUCCESS]: (state, { payload: currentUser }) => ({
    ...state,
    currentUser,
    signInValues: {},
    isSigningIn: false,
  }),
  [SIGN_IN_FAILURE]: (state, { payload: { error } }) => ({
    ...state,
    error,
    isSigningIn: false,
  }),
}, initialState);
