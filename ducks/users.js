import { handleActions, createAction } from 'redux-actions';
import { map } from 'lodash';
import { CALL_API } from 'redux-api-middleware';
import backend, { endpoint, headers } from 'utils/backend';

export const CLEAR = 'app/users/CLEAR';

export const FETCH_ALL = 'users/FETCH_ALL';
export const FETCH_ALL_SUCCESS = 'users/FETCH_ALL_SUCCESS';
export const FETCH_ALL_FAILURE = 'users/FETCH_FAILURE';

export const CHANGE_USER_ROLE_REQUEST = 'app/users/CHANGE_USER_ROLE_REQUEST';
export const CHANGE_USER_ROLE_SUCCESS = 'app/users/CHANGE_USER_ROLE_SUCCESS';
export const CHANGE_USER_ROLE_FAILURE = 'app/users/CHANGE_USER_ROLE_FAILURE';

export const INVITE_NEW_CLIENT_REQUEST = 'app/users/INVITE_NEW_CLIENT_REQUEST';
export const INVITE_NEW_CLIENT_SUCCESS = 'app/users/INVITE_NEW_CLIENT_SUCCESS';
export const INVITE_NEW_CLIENT_FAILURE = 'app/users/INVITE_NEW_CLIENT_FAILURE';

export const fetchUsers = createAction(FETCH_ALL);
export const fetchUsersSuccess = createAction(FETCH_ALL_SUCCESS, users => users);
export const fetchUsersFailure = createAction(FETCH_ALL_FAILURE, error => error);

const initialState = {
  items: [],
  isFetching: false,

  inviteNewClientInProgress: false,
  inviteNewClientValues: {},
};

export default handleActions({
  [CLEAR]: state => initialState,
  [FETCH_ALL]: (state) => {
    return {
      ...state,
      items: [],
      isFetching: true,
    };
  },
  [FETCH_ALL_SUCCESS]: (state, { payload }) => {
    return {
      ...state,
      items: payload,
      isFetching: false,
    };
  },
  [FETCH_ALL_FAILURE]: (state, { payload: { error } }) => ({
    ...state,
    error,
    isFetching: false,
  }),

  [CHANGE_USER_ROLE_SUCCESS]: (state, { payload: { userId, role } }) => ({
    ...state,
    items: map(state.items, item => item._id == userId ? { ...item, role } : item),
  }),

  [INVITE_NEW_CLIENT_REQUEST]: (state) => ({
    ...state,
  }),
  [INVITE_NEW_CLIENT_SUCCESS]: (state) => ({
    ...state,

  }),
  [INVITE_NEW_CLIENT_FAILURE]: (state) => ({
    ...state,

  })

}, initialState);

export function clearUsers() {
  return {
    type: CLEAR,
  };
};


// export function fetchUsers() {
//   return {
//     [CALL_API]: {
//       endpoint: endpoint('/users'),
//       method: 'GET',
//       headers: headers(),
//       types: [ FETCH, FETCH_SUCCESS, FETCH_FAILURE ],
//     },
//   };
// };

export function changeUserRole(userId, role, onSuccess, onError) {
  return {
    [CALL_API]: {
      endpoint: endpoint(`users/${userId}/role`),
      method: 'POST',
      headers: headers(),
      body: JSON.stringify({ role }),
      types: [
        CHANGE_USER_ROLE_REQUEST,
        { type: CHANGE_USER_ROLE_SUCCESS,
          payload: () => {
            if (typeof onSuccess === 'function') { onSuccess() }
            return { userId, role };
          }
        },
        CHANGE_USER_ROLE_FAILURE,
      ],
    },
  };
}

export function inviteNewClient(user, onSuccess) {
  return {
    [CALL_API]: {
      endpoint: endpoint('users'),
      method: 'POST',
      headers: headers(),
      body: JSON.stringify({ user }),
      types: [
        {
          type: INVITE_NEW_CLIENT_REQUEST,
          payload: user,
        },
        {
          type: INVITE_NEW_CLIENT_SUCCESS,
          payload: (action, state, res) => {
            return res.json().then((json) => {
              if (typeof onSuccess === 'function') { onSuccess(json) }

              return json;
            })
          }
        },
        INVITE_NEW_CLIENT_FAILURE,
      ],
    },
  };
}

export function removeUser(userId, onSuccess) {
  return {
    [CALL_API]: {
      endpoint: endpoint(`users/${userId}`),
      method: 'DELETE',
      headers: headers(),
      types: [
        'NOPE',
        {
          type: 'NOPE',
          meta: (action, state, res) => {
            if (typeof onSuccess === 'function') { onSuccess() }

            if (res) {
              return {
                status: res.status,
                statusText: res.statusText,
              };
            } else {
              return {
                status: 'Network request failed',
              }
            }
          }
        },
        'NOPE',
      ]
    }
  }
}
