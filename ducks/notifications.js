import { handleActions } from 'redux-actions';
import { filter } from 'lodash';

const ADD = 'app/notifications/add';
const REMOVE = 'app/notifications/remove';

export default handleActions({
  [ADD]: (state, { payload: { notification } }) => [notification, ...state],
  [REMOVE]: (state, { payload: { key } }) => (filter(state, n => n.key !== key))
}, []);

let uid = 0;

export function addNotification(message, options={}) {
  return (dispatch) => {
    uid += 1;
    const id = uid;

    dispatch({
      type: ADD,
      payload: {
        notification: {
          key: id,
          message,
          style: options.style || 'success',
          title: options.title,
          onDismiss: () => dispatch(removeNotification(id)),
        },
      },
    });

    setTimeout(() => dispatch(removeNotification(id)), 3000);
  }
}

export function removeNotification(key) {
  return {
    type: REMOVE,
    payload: {
      key,
    },
  };
}
