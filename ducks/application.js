import { handleActions } from 'redux-actions';
import { filter, findIndex, map, result } from 'lodash';
import { CALL_API, getJSON } from 'redux-api-middleware';
import { headers, endpoint } from 'utils/backend';

import _ from 'lodash';
import axios from 'axios';
axios.defaults.headers.common['Authorization'] = localStorage.getItem('jwt');

const CLEAR = 'app/application/CLEAR';
const MERGE_APPLICATION_WITH_RECORD = 'app/application/MERGE_APPLICATION_WITH_RECORD';

const FETCH_REQUEST = 'app/application/FETCH_REQUEST';
const FETCH_SUCCESS = 'app/application/FETCH_SUCCESS';
const FETCH_FAILURE = 'app/application/FETCH_FAILURE';

const UPDATE_REQUEST = 'app/application/UPDATE_REQUEST';
const UPDATE_SUCCESS = 'app/application/UPDATE_SUCCESS';
const UPDATE_FAILURE = 'app/application/UPDATE_FAILURE';

const initialState = {
  item: {
    applicants: []
  },
  isFetching: false,
}

export default handleActions({
  [CLEAR]: (state) => (initialState),
  [MERGE_APPLICATION_WITH_RECORD]: (state, action) => ({
    ...state,
    item: {
      ...state.item,
      ...action.item,
      applicants: state.item.applicants.map((applicant, index) => ({
        ...applicant,
        ...(action.item.applicants[index] || {})
      }))
    }
  }),
  [FETCH_REQUEST]: (state, action) => {
    return {
      ...state,
      isFetching: true,
      error: null,
      item: {},
    };
  },
  [FETCH_SUCCESS]: (state, { payload: { data } }) => {
    return {
      ...state,
      isFetching: false,
      item: data,
    };
  },
  [FETCH_FAILURE]: (state, { payload: { error } }) => {
    return {
      ...state,
      isFetching: false,
      error,
    };
  },
}, initialState);

let applicationRef;

const wait = (ms) => new Promise((resolve) => { setTimeout(resolve, ms) });

export function fetchApplication(id, onSuccess) {
  return async (dispatch) => {
    try {
      dispatch({ type: FETCH_REQUEST });

      const response = await axios.get(endpoint(`/applications/${id}`));
      const data = result(response.data, 'data', {});
      dispatch({
        type: FETCH_SUCCESS,
        payload: {
          data,
        }
      });
    } catch (error) {
      console.dir(error);
      dispatch({
        type: FETCH_FAILURE,
        payload: {
          error,
        }
      })
    }
  }
  // return {
  //   [CALL_API]: {
  //     endpoint: `/api/applications/${id}`,
  //     method: 'GET',
  //     headers: headers(),
  //     types: [
  //       FETCH_REQUEST,
  //       { type: FETCH_SUCCESS,
  //         payload: (action, state, res) => {
  //           if (typeof onSuccess === 'function') { onSuccess() }
  //           return getJSON(res);
  //         }
  //       },
  //       FETCH_FAILURE ],
  //   },
  // };
}

export function fetchRecord(id) {
  return async dispatch => {
    try {
      const response = await axios.get(`/api/zoho/${id}`);
      const data = _.result(response, 'data.data.response.result.Potentials.row.FL');
      const normalizedData = _.mapValues(_.mapKeys(data, value => value.val), value => value.content);

      console.log(normalizedData);

      const targetVals = {
        'applicants[0].first_name': _.first(_.split(_.result(normalizedData, 'Potential Owner'), /\s+/)),
        'applicants[0].surname': _.last(_.split(_.result(normalizedData, 'Potential Owner'), /\s+/)),
        'additional_comments': _.result(normalizedData, 'Description'),
      };

      _.map(targetVals, (value, key) => {
        dispatch({
        type: 'redux-form/CHANGE',
        meta: {
          form: 'application-form',
          field: key
        },
        payload: value
      })
    });
    } catch (error) {
      console.log('not fetched record', error);
    }
  }
}

export function updateApplication(id, application, onSuccess) {
  console.log('updateApplication', id, application)
  return {
    [CALL_API]: {
      endpoint: endpoint(`/applications/${id}`),
      method: 'PATCH',
      headers: headers(),
      body: JSON.stringify({ application } ),
      types: [
        UPDATE_REQUEST,
        {
          type: UPDATE_SUCCESS,
          payload: (action, state, res) => {
            const json = getJSON(res);
            if (typeof onSuccess === 'function') {
              json.then((app) => onSuccess(app))
            }
            return json;
          }
        },
        UPDATE_FAILURE,
      ]
    }
  }
}
