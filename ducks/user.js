import { handleActions } from 'redux-actions';
import { CALL_API } from 'redux-api-middleware';
import backend, { headers, endpoint } from 'utils/backend';

const CLEAR = 'app/user/CLEAR';

const FETCH = 'app/user/FETCH';
const FETCH_SUCCESS = 'app/user/FETCH_SUCCESS';
const FETCH_FAILURE = 'app/user/FETCH_FAILURE';

const UPDATE = 'app/users/UPDATE';
const UPDATE_SUCCESS = 'app/user/UPDATE_SUCCESS';
const UPDATE_FAILURE = 'app/user/UPDATE_FAILURE';

const initialState = {
  item: {},
  isFetching: false,
};


export default handleActions({
  [CLEAR]: state => initialState,
  [FETCH]: (state) => {
    return {
      ...state,
      item: {},
      isFetching: true,
    };
  },
  [FETCH_SUCCESS]: (state, { payload: { data: user }}) => {
    return {
      ...state,
      item: user,
      isFetching: false,
    };
  },
  [FETCH_FAILURE]: (state, { payload: { error } }) => ({
    ...state,
    error,
    isFetching: false,
  }),

  [UPDATE]: (state) => {
    return {
      ...state,
      isUpdating: true,
    };
  },
  [UPDATE_SUCCESS]: (state, { payload: { data: user }}) => {
    return {
      ...state,
      item: user,
      isUpdating: false,
    };
  },
  [UPDATE_FAILURE]: (state, { payload: { error } }) => ({
    ...state,
    error,
    isUpdating: false,
  }),
}, initialState)

export function clearUser() {
  return {
    type: CLEAR,
  }
}

export function fetchUser(userId) {
  return {
    [CALL_API]: {
      endpoint: endpoint(`users/${userId}`),
      method: 'GET',
      headers: headers(),
      types: [
        FETCH,
        FETCH_SUCCESS,
        FETCH_FAILURE
      ],
    },
  };
};

export function updateUser(userId, data, onSuccess) {
  return {
    [CALL_API]: {
      endpoint: endpoint(`users/${userId}`),
      method: 'PATCH',
      headers: headers(),
      body: JSON.stringify({
        user: data,
      }),
      types: [
        UPDATE,
        { type: UPDATE_SUCCESS,
          payload: (action, state, res) => {
            return res.json().then((json) => {
              if (typeof onSuccess === 'function') { onSuccess(json) }

              return json;
            });
          }
        },
        UPDATE_FAILURE
      ],
    },
  };
};
