import { combineReducers } from 'redux';

import { reducer as form } from 'redux-form';
import auth from 'ducks/auth';
import applications from 'ducks/applications';
import application from 'ducks/application';
import users from 'ducks/users';
import user from 'ducks/user';
import documents from 'ducks/documents';
import notifications from 'ducks/notifications';

export const rootReducer = combineReducers({
  form,
  auth,
  applications,
  application,
  users,
  user,
  documents,
  notifications,
});
