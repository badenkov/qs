import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  signInWithEmailAndPassword,
} from 'ducks/auth';

import { Redirect } from 'react-router';
import {
  Grid,
  Row,
  Col,
  Button
} from 'react-bootstrap';
import { Link } from 'react-router';
import Spinner from 'react-spinkit';
import SignInForm from 'forms/SignInForm';

@connect(
  ({ auth }) => ({ auth }),
  {
    signInWithEmailAndPassword,
  }
)
export default class SignIn extends Component {
  render() {
    const { auth } = this.props;

    if (auth.currentUser) {
      return <Redirect to="/" />;
    }

    return (
      <div>
        {this.props.auth.error && (
          <div className="alert alert-danger">
            {this.props.auth.error}
          </div>
        )}
        <SignInForm
          initialValues={this.props.signInValues}
          disabled
          ref={(node) => this.form = node}
          onSubmit={({ email, password }) => {
            this.props.signInWithEmailAndPassword(email, password);
          }} />

        <hr />
        <Row>
          <Col md={9} mdOffset={3}
               sm={9} smOffset={3}>
            <Button
              disabled={this.props.auth.isSigningIn}
              bsStyle="success"
              onClick={() => this.form.submit()}>
              {this.props.auth.isSigningIn ? (
                <i className="fa fa-spinner fa-spin" />
              ) : "Sign in"}
            </Button>

            <br /><br />
            Do not have an account yet?&nbsp;
            <Link to="/sign-up">Sign up right now</Link>
          </Col>
        </Row>

      </div>
    );
  }
}
