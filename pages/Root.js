import React, { Component } from 'react';
import { connect } from 'react-redux';1
import { Match, Miss, Redirect } from 'react-router';
import RequireAuth from 'containers/RequireAuth';

import Layout from 'containers/Layout';
import NotFound from 'pages/NotFound';
import SignIn from 'pages/SignIn';

import Applications from 'pages/Applications';
import Users from 'pages/Users';
import Sandbox from 'pages/Sandbox';

@connect(({ auth: { initialized, currentUser } }) => ({ initialized, currentUser }))
export default class Root extends Component {
  render() {
    if (!this.props.initialized) {
      return (<div>Loading...</div>);
    }

    if (this.props.currentUser) {
      return (
        <Layout>
          <Match pattern="/" exactly render={() => <Redirect to="/applications" />} />

          <Match pattern="/sign-in" component={SignIn} />

          <Match pattern="/applications" component={Applications} />
          <Match pattern="/users" component={Users} />
          <Match pattern="/sandbox" component={Sandbox} />

          <Miss component={NotFound} />
        </Layout>
      );
    } else {
      return (
        <Layout>
          <Match pattern="/" exactly render={() => <Redirect to="/sign-in" />} />

          <Match pattern="/sign-in" component={SignIn} />

          <Miss render={() => <Redirect to="/sign-in" />} />
        </Layout>
      );
    }
  }
}
