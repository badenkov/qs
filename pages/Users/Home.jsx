import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import {
  Table,
  ButtonGroup,
  Button,
} from 'react-bootstrap';
import {
  fetchUsers,
  clearUsers,
  changeUserRole,
  removeUser,
} from 'ducks/users';
import {
  addNotification
} from 'ducks/notifications';
import { map } from 'lodash';
import Can from 'containers/Can';

@connect(
  ({ users }) => ({ users }),
  {
    fetchUsers,
    clearUsers,
    changeUserRole,
    removeUser,
    addNotification,
  }
)
export default class Users extends Component {
  componentDidMount() {
    this.props.fetchUsers();
  }

  componentWillUnmount() {
    this.props.clearUsers();
  }

  render() {
    const {
      users: {
        items,
        isFetching,
      }
    } = this.props;

    if (isFetching) {
      return (
        <div>Loading...</div>
      );
    }

    return (
      <Table striped bordered condensed hover>
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Role</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {map(items, (user) => <tr key={`user-${user.id}`}>
            <td>{user.first_name}</td>
            <td>{user.last_name}</td>
            <td>{user.email}</td>
            <td>{user.role}</td>
            <td>
              {!user.currentUser && (
                <div className="pull-right">
                  <ButtonGroup>
                    <Can type="user.update" model={user}>
                      <Link to={`users/edit/${user.id}`}>
                        {({ onClick, href }) => (
                          <Button bsSize="xsmall" bsStyle="primary" onClick={onClick}>
                            <i className="fa fa-edit" />
                          </Button>
                        )}
                      </Link>
                    </Can>

                    <Can type="user.destroy" model={user}>
                      <Button bsSize="xsmall" bsStyle="danger" onClick={() => {
                        this.props.removeUser(user.id, () => {
                          this.props.fetchUsers();
                          this.props.addNotification('User deleted!')
                        });
                      }}>
                        <i className="fa fa-remove"/>
                      </Button>
                    </Can>

                  </ButtonGroup>
                </div>
              )}
            </td>
          </tr>)}
        </tbody>
      </Table>
    );
  }
}
