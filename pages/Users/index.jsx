import React from 'react';
import { Match, Link } from 'react-router';
import { Nav, NavItem } from 'react-bootstrap';

import Home from './Home';
import InviteNewClient from './InviteNewClient';
import EditUser from './EditUser';

const Users = ({ pathname }) => (
  <div>
    <Nav bsStyle="pills">
      <Link to={`${pathname}`} activeOnlyWhenExact>
        {({ isActive, href, onClick }) => (
          <NavItem active={isActive} href={href} onClick={onClick}>Users</NavItem>
        )}
      </Link>
      <Link to={`${pathname}/invite`}>
        {({ isActive, href, onClick }) => (
          <NavItem active={isActive} href={href} onClick={onClick}>Invite new client</NavItem>
        )}
      </Link>

      <Match pattern={`${pathname}/edit/:userId`} render={({ params: { userId } }) => (
        <NavItem active >Edit user №{userId}</NavItem>
      )} />
    </Nav>
    <hr />

    <Match pattern={pathname} exactly component={Home} />
    <Match pattern={`${pathname}/invite`} component={InviteNewClient} />
    <Match pattern={`${pathname}/edit/:userId`} component={EditUser} />
  </div>
);

export default Users;
