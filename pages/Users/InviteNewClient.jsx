import React, { Component } from 'react';
import { connect } from 'react-redux';
import { inviteNewClient } from 'ducks/users';
import { addNotification } from 'ducks/notifications';
import {
  Button,
} from 'react-bootstrap';
import { Redirect } from 'react-router';
import UserForm from 'forms/UserForm';

@connect(
  (state) => ({ state }),
  {
    inviteNewClient,
    addNotification,
  }
)
export default class InviteNewClient extends Component {
  state = {
    invited: false,
  }

  render() {
    if (this.state.invited) {
      return <Redirect to="/users" />;
    }

    return (
      <div>

      <UserForm
        onSubmit={(data) => this.props.inviteNewClient(data, () => {
          this.props.addNotification('We sent email to client')
          this.setState({ invited: true });
        })}
        ref={(form) => this._userForm = form} />

      </div>
    );
  }
}
