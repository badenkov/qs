import React, { Component } from 'react';
import { connect } from 'react-redux';
import { clearUser, fetchUser, updateUser } from 'ducks/user';
import { addNotification } from 'ducks/notifications';
import {
  Button,
} from 'react-bootstrap';
import { Redirect } from 'react-router';
import UserForm from 'forms/UserForm';

@connect(
  ({ user: { isFetching, isUpdating, item } }) => ({
    isFetching,
    isUpdating,
    item,
  }),
  {
    clearUser,
    fetchUser,
    updateUser,
    addNotification,
  }
)
export default class EditUser extends Component {
  state = {
    saved: false,
  }

  componentDidMount() {
    this.props.fetchUser(this.props.params.userId);
  }

  componentWillUnmount() {
    this.props.clearUser();
  }

  render() {
    if (this.state.invited) {
      return <Redirect to="/users" />;
    }

    if (this.props.isFetching) {
      return <p>Loading</p>;
    }

    return (
      <div>

        <UserForm
          initialValues={this.props.item}
          onSubmit={(data) => this.props.updateUser(this.props.params.userId, data, () => {
            this.props.addNotification('User updated successfully')
            this.setState({ saved: true });
          })}
          ref={(form) => this._userForm = form} />

      </div>
    );
  }
}
