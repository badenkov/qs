import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import axios from 'axios';

export default class Sandbox extends Component {
  state = {
    uploading: false,
    response: {},
  }

  constructor(props) {
    super(props);
    this.onDrop = this.onDrop.bind(this);
  }

  onDrop(files) {
    this.setState({ uploading: true });
    const data = new FormData();
    data.append('file', files[0]);

    axios.post('http://localhost:4000/api/upload', data, {
      onUploadProgress: (evt) => {
        console.log('upload progress', evt);
      },
    }).then((res) => {
      console.log(res);
      this.setState({ uploading: false });
    }).catch((err) => {
      console.log(err);
      this.setState({ uploading: false });
    });
  }

  render() {
    return (
      <div>
        <h1>Development sandbox</h1>

        <button onClick={() => axios.get('/api/zoho/431584000012660027').then((res) => this.setState({ response: res.data.data }))}>Get zoho record</button>
        <pre>{JSON.stringify(this.state.response, null, 2)}</pre>

        {!this.state.uploading ? (
          <Dropzone multiple={false} onDrop={this.onDrop}>
            <div>Drop file here, or click to select files to upload.</div>
          </Dropzone>
        ) : (
          <p>Uploading</p>
        )}
      </div>
    );
  }
}
