import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  PageHeader,
  Row,
  Col,
  ButtonGroup,
  Button,
  Table,
} from 'react-bootstrap';
import Spinner from 'react-spinkit';
import Application from 'components/Application';
import { Link } from 'react-router';
import {
  fetchApplications,
  removeApplication,
} from 'ducks/applications';
import axios from 'axios';

@connect(
  ({ applications }) => ({ applications }),
  {
    fetchApplications,
    removeApplication,
  }
)
export default class Applications extends Component {
  componentDidMount() {
    this.props.fetchApplications();
  }

  render() {
    const { applications } = this.props;

    if (applications.isFetching) {
      return <Spinner />
    }

    return (
      <div>
        {applications.items.map((app, index) => (
          <Application
            key={`application1-${app.id}`}
            application={app}
            onDestroy={() => {
              if (confirm('Are you sure?')) {
                this.props.removeApplication(app.id, () => this.props.fetchApplications())
              }
            }} />
        ))}
      </div>
    );
  }
}
