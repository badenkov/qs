import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Button,
} from 'react-bootstrap';
import DocumentForm from 'forms/DocumentForm';
import { submit } from 'redux-form';
import { createDocument } from 'ducks/documents';

@connect(
  (state) => ({ t: 1 }),
  {
    submit,
    createDocument,
  }
)
export default class NewDocument extends Component {
  render() {
    return (
      <div>
        <h1>Create new document</h1>

        <DocumentForm
          onSubmit={(values) => this.props.createDocument(values, () => console.log('hi'))}
          />

        <Button onClick={() => this.props.submit('document-form')}>Save</Button>
      </div>
    );
  }
}
