import React, { Component } from 'react';
import { connect } from 'react-redux';
import { submit, reset } from 'redux-form';
import {
  createDocument,
  updateDocument,
  uploadDocument,
  destroyDocument,
  fetchDocuments,
} from 'ducks/documents';
import {
  PageHeader,
  Row,
  Col,
  Button,
  ButtonGroup,
  Alert,
  Table,
} from 'react-bootstrap';
import { Link, Match } from 'react-router';

import DocumentList from 'containers/Applications/Documents/list';
import DocumentForm from 'forms/DocumentForm';
import Can from 'containers/Can';

@connect(
  (state) => ({
    documents: state.documents,
  }),
  {
    reset,
    submit,
    createDocument,
    updateDocument,
    uploadDocument,
    destroyDocument,
    fetchDocuments
  }
)
export default class Documents extends Component {
  state = {
    isCreating: false,
  }

  componentDidMount() {
    this.props.fetchDocuments(this.props.params.applicationId);
  }

  render() {
    const {
      pathname,
      params: {
        applicationId,
      },
      documents: {
        items,
        isFetching,
      },
    } = this.props;

    return (
      <Row>
        <Col lg={12} md={12}>
          <ul className="nav nav-tabs">
            <Link to={`/applications/edit/${applicationId}`}>{({ isActive, onClick, href }) => (
              <li role="presentation" className={isActive ? 'active' : ''}>
                <a href={href} onClick={onClick}>Application</a>
              </li>
            )}</Link>
          <Link to={`/applications/documents/${applicationId}`}>{({ isActive, onClick, href }) => (
              <li role="presentation" className={isActive ? 'active' : ''}>
                <a href={href} onClick={onClick}>Documents</a>
              </li>
            )}</Link>
          </ul>

          <h1>Which documents does the client need to upload?</h1>

          <Can type="documents.create" model={document}>
            <div>
              <DocumentForm
                onSubmit={(values) => {
                  this.setState({ isCreating: true });
                  this.props.createDocument(applicationId, values,
                    () => {
                      this.props.reset('document-form');
                      this.props.fetchDocuments(applicationId);
                      this.setState({ isCreating: false });
                    },
                    () => {
                      this.setState({ isCreating: false });
                    })}}
                />

              <Row>
                <Col sm={9} smOffset={3}>
                  <Button
                    disabled={this.state.isCreating}
                    onClick={() => this.props.submit('document-form')}>
                    {this.state.isCreating ? (
                      <i className="fa fa-spinner fa-spin" />
                    ) : "Add"}
                  </Button>
                </Col>
              </Row>
              <hr />
            </div>
          </Can>

          <DocumentList applicationID={parseInt(applicationId)} />

        </Col>
      </Row>
    )
  }
}
