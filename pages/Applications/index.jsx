import React from 'react';
import { Match, Link } from 'react-router';
import { Nav, NavItem } from 'react-bootstrap';

import Applications from './Applications';
import Application from './Application';
import NewApplication from './NewApplication';
import Documents from './Documents';

export default ({ pathname }) => (
  <div>
    <Nav bsStyle="pills">
      <Link to={`${pathname}`} activeOnlyWhenExact>
        {({ isActive, href, onClick }) => (
          <NavItem active={isActive} href={href} onClick={onClick}>Applications</NavItem>
        )}
      </Link>
      <Link to={`${pathname}/new`}>
        {({ isActive, href, onClick }) => (
          <NavItem active={isActive} href={href} onClick={onClick}>New application</NavItem>
        )}
      </Link>
      <Match pattern={`${pathname}/edit/:applicationId`} render={({ params: { applicationId } }) => (
        <NavItem active >Edit application №{applicationId}</NavItem>
      )} />
    </Nav>
    <hr />

    <Match pattern={pathname} exactly component={Applications} />
    <Match pattern={`${pathname}/new`} component={NewApplication} />
    <Match pattern={`${pathname}/edit/:applicationId`} component={Application} />
    <Match pattern={`${pathname}/documents/:applicationId`} component={Documents} />
  </div>
);
