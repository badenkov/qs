import React, { Component } from 'react';
import { connect } from 'react-redux';
import { PageHeader, Row, Col, Button, ButtonGroup, Alert } from 'react-bootstrap';
import { Link } from 'react-router';
import ApplicationForm from 'forms/ApplicationForm';
import Spinner from 'react-spinkit';
import {
  updateApplication,
  fetchApplication,
  fetchRecord,
} from 'ducks/application';
import axios from 'axios';

const PDF_SERVICE = window.PDF_SERVICE;

@connect(
  (state) => ({
    application: state.application,
    form: state.form['application-form']
  }),
  {
    updateApplication,
    fetchApplication,
    fetchRecord,
  }
)
export default class Application extends Component {
  constructor() {
    super();
    this.state = {
      loadingPdf: false
    };
  }

  componentDidMount() {
    const {
      params: {
        applicationId
      },
      // location: {
      //   query: {
      //     record_id
      //   }
      // }
    } = this.props;

    this.props.fetchApplication(applicationId).then(() => {
      console.log('fetched!!!!');
    });
    // if (record_id) {
    //   this.props.fetchRecord(record_id);
    // }
  }

  requestPdf() {
    const {
      form: {
        values = {}
      } = {},
      params: {
        applicationId
      }
    } = this.props;

    // console.log(values);

    this.setState({
      loadingPdf: true
    });

    axios.post(PDF_SERVICE, {
      applicationId,
      ...values
    }, {
      responseType: 'arraybuffer'
    }).then(response => {
      const blob = new Blob([response.data], {
        type: 'application/pdf'
      });
      const reader = new FileReader();

      reader.addEventListener('loadend', () => {
        window.open(reader.result, '_blank');
      });

      reader.readAsDataURL(blob);

      this.setState({
        loadingPdf: false
      });
    }).catch(() => {
      this.setState({
        loadingPdf: false
      });
    });
  }

  render() {
    const {
      params: {
        applicationId,
      },
      application: {
        item,
        isFetching,
        error,
      },
      children
    } = this.props;
    const {
      loadingPdf
    } = this.state;

    if (isFetching) {
      return <Spinner />;
    }

    if (error) {
      return (
        <div className="container-fluid">
          <Row>
            <Col lg={12} md={12}>
              <Alert bsStyle="danger">
                {error.message}
              </Alert>
            </Col>
          </Row>
        </div>
      )
    }

    return (
      <Row>
        <Col lg={12} md={12}>
          <div style={{ position: "fixed", top: 0, right: 0 }}>
            <ButtonGroup>
              <Link to="/applications" activeOnlyWhenExact>{
                ({ onClick }) => <Button onClick={onClick}>Cancel</Button>
              }</Link>

              <Button
                bsStyle="success"
                onClick={() => {
                  this.form.submit()
                }}
              >
                Save
              </Button>
              <Button
                bsStyle={loadingPdf ? 'default' : 'primary'}
                disabled={loadingPdf}
                onClick={() => this.requestPdf()}
              >
                {loadingPdf ? <Spinner spinnerName="circle" /> : 'PDF'}
              </Button>
            </ButtonGroup>
          </div>

          <ul className="nav nav-tabs">
            <Link to={`/applications/edit/${applicationId}`}>{({ isActive, onClick, href }) => (
              <li role="presentation" className={isActive ? 'active' : ''}>
                <a href={href} onClick={onClick}>Application</a>
              </li>
            )}</Link>
          <Link to={`/applications/documents/${applicationId}`}>{({ isActive, onClick, href }) => (
              <li role="presentation" className={isActive ? 'active' : ''}>
                <a href={href} onClick={onClick}>Documents</a>
              </li>
            )}</Link>
          </ul>

          <ApplicationForm
            ref={node => {
              this.form = node;
            }}
            initialValues={item}
            onSubmit={(data) => {
              this.props.updateApplication(applicationId, data);
            }}
          />

          <hr />

          <div className="pull-right">
            <Button
              bsStyle="success"
              onClick={() => this.form.submit()}
            >
              Save
            </Button>
          </div>

          {children}
        </Col>
      </Row>
    );
  }
}
