import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Row,
  Col,
  PageHeader,
} from 'react-bootstrap';
import { Link } from 'react-router';
import ApplicationForm from 'forms/ApplicationForm';
import { createApplication } from 'ducks/applications';
import { fetchRecord } from 'ducks/application';

@connect(
  (state) => ({ state }),
  {
    createApplication,
    fetchRecord
  }
)
export default class NewApplication extends Component {
  static contextTypes = {
    router: PropTypes.object.isRequired
  }

  constructor() {
    super();
    this.form = {
      submit: () => null
    };
  }

  componentWillMount() {
    if (this.props.location && this.props.location.query) {
      const { record_id } = this.props.location.query;

      if (record_id) {
        this.props.fetchRecord(record_id);
      }
    }
  }

  handleSubmit(data) {
    this.props.createApplication(data, () => {
      this.context.router.replaceWith('/applications');
    });
  }

  render() {
    return (
      <div>
        <ApplicationForm
          initialValues={{ applicants: [{}]}}
          ref={node => {
            this.form = node;
          }}
          onSubmit={(data) => this.handleSubmit(data)}
        />

        <Button bsStyle="success" onClick={() => this.form.submit()}>Save</Button>
        <Link to="/applications" activeOnlyWhenExact>{
          ({ onClick }) => <Button onClick={onClick}>Cancel</Button>
        }</Link>
      </div>
    );
  }
}
