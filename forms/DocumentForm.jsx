import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { map, pick } from 'lodash';
import validatejs from 'validate.js';
import {
  Form,
  Button,
  Row,
  Col,
} from 'react-bootstrap';

import Input from 'components/input';

@reduxForm({
  form: 'document-form',
})
export default class DocumentForm extends Component {
  render() {
    const {
      handleSubmit
    } = this.props;

    return (
      <Form horizontal onSubmit={handleSubmit}>
        <Field
          name="name"
          component={Input}
          type="text"
          label="Document Type" />
      </Form>
    );
  }
}
