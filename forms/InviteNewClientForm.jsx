import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import validatejs from 'validate.js';
import {
  Form,
  Button,
  Row,
  Col,
} from 'react-bootstrap';

import Input from 'components/input';
import { Link } from 'react-router';

const validate = (fields) => {
  const defaultFields = {
    email: '',
    first_name: '',
    last_name: '',
  };

  const constraints = {
    email: {
      presence: true,
      email: true,
    },
    first_name: {
      presence: true,
    },
    last_name: {
      presence: true,
    }
  };

  return validatejs({...defaultFields, ...fields}, constraints) || {};
}

@reduxForm({
  form: 'invite-new-client-form',
  validate,
})
export default class InviteNewClientForm extends Component {
  render() {
    const { handleSubmit } = this.props;

    return (
      <Form horizontal className="form-login" onSubmit={handleSubmit}>
        <Field
          name="email"
          component={Input}
          type="text"
          label="Email" />

        <Field
          name="first_name"
          component={Input}
          type="text"
          label="First Name" />

        <Field
          name="last_name"
          component={Input}
          type="text"
          label="Last Name" />

        <hr />
        <Row>
          <Col md={9} mdOffset={3}>
            <Button bsStyle="success" type="submit">Invite</Button>
          </Col>
        </Row>
      </Form>
    )
  }
}
