import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field, formValueSelector } from 'redux-form';
import { map, pick } from 'lodash';
import validatejs from 'validate.js';
import {
  Form,
  Button,
  Row,
  Col,
} from 'react-bootstrap';

import Input from 'components/input';
import PermittedAttribute from 'containers/PermittedAttribute';
import { Link } from 'react-router';

const validate = (fields, form) => {
  const constraints = {
    email: {
      presence: true,
      email: true,
    },
    first_name: {
      presence: true,
    },
    last_name: {
      presence: true,
    },
    role: {
      presence: true,

    }
  };
  const permitted_attributes = map(form.registeredFields, f => f.name);

  console.log(pick(constraints, permitted_attributes));

  return validatejs(fields, constraints) || {};
}

const selector = formValueSelector('user-form');
@connect((state) => ({
  role: selector(state, 'role'),
}))
@reduxForm({
  form: 'user-form',
  validate,
})
export default class UserForm extends Component {
  render() {
    const { handleSubmit } = this.props;

    const model = this.props.initialValues;

    return (
      <Form horizontal className="form-login" onSubmit={handleSubmit}>
        <PermittedAttribute type="user" model={model} attribute="email">
          <Field
            name="email"
            component={Input}
            type="email"
            label="Email" />
        </PermittedAttribute>

        <PermittedAttribute type="user" model={model} attribute="first_name">
          <Field
            name="first_name"
            component={Input}
            type="text"
            label="First Name" />
        </PermittedAttribute>

        <PermittedAttribute type="user" model={model} attribute="last_name">
          <Field
            name="last_name"
            component={Input}
            type="text"
            label="Last Name" />
        </PermittedAttribute>

        {this.props.role === 'broker' && (
          <PermittedAttribute type="user" model={model} attribute="bio">
            <Field
              name="bio"
              component={Input}
              type="textarea"
              label="Bio" />
          </PermittedAttribute>
        )}

        <PermittedAttribute type="user" model={model} attribute="password">
          <Field
            name="password"
            component={Input}
            type="password"
            label="Password" />
        </PermittedAttribute>

        <PermittedAttribute type="user" model={model} attribute="role">
          <Field
            name="role"
            component={Input}
            type="select"
            options={["admin", "broker", "client"]}
            label="Role" />
        </PermittedAttribute>

        <hr />
        <Row>
          <Col md={9} mdOffset={3}>
            <Button bsStyle="success" type="submit">Save</Button>
          </Col>
        </Row>
      </Form>
    )
  }
}
