import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import {
  Form,
} from 'react-bootstrap';
import Input from 'components/input';
import { Link } from 'react-router';

class SignInForm extends Component {
  render() {
    const { handleSubmit } = this.props;

    return (
      <Form horizontal className="form-login" onSubmit={handleSubmit}>
        <Field
          name="email"
          component={Input}
          type="text"
          label="Email"
        />

        <Field
          name="password"
          component={Input}
          type="password"
          label="Password"
        />

      </Form>
    );
  }
}

export default reduxForm({
  form: 'signup'
})(SignInForm);
