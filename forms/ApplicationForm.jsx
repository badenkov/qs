import React, { Component } from 'react';
import {
  Row,
  Col,
  Form,
  Button,
  Table
} from 'react-bootstrap';
import {
  Field,
  FieldArray,
  reduxForm,
  formValueSelector,
  arrayPush
} from 'redux-form';
import _ from 'lodash';
import moment from 'moment';
import { connect } from 'react-redux';
import Input from 'components/input';
import UsersSelect from 'components/UsersSelect';
import StatusSelect from 'components/StatusSelect';

import PermittedAttribute from 'containers/PermittedAttribute';
import CopyValue from 'containers/CopyValue';

const validate = (values) => {
  const {
    applicants = []
  } = values;
  const nonEmpty = value => !_.isFinite(value) && _.isEmpty(value) && 'This field cannot be empty';
  const validEmail = value => (/^.+?@.+?\..+?$/i.test(value) ? null : 'Please enter valid email address');
  const earlierThan = (date, diff = [3, 'years']) => moment()
    .subtract(...diff)
    .add(1, 'day')
    .startOf('day')
    .isBefore(date);
  const checkPreviousAddresses = (addresses) => _.map(addresses, address => ({
    address: nonEmpty(address.address),
    date_moved_into_address: nonEmpty(address.date_moved_into_address)
  }));
  const checkLastAddress = (addresses) => {
    const every = _.every(addresses, address => earlierThan(address.date_moved_into_address, [3, 'years']));

    return every ? 'The earliest has to be at least 2 years ago' : nonEmpty(_.last(addresses));
  };

  const errors = {
    applicants: applicants.map(applicant => ({
      first_name: nonEmpty(applicant.first_name),
      surname: nonEmpty(applicant.surname),
      date_of_birth: nonEmpty(applicant.date_of_birth),
      phone_home: nonEmpty(applicant.phone_home),
      email_address: validEmail(applicant.email_address),
      current_address: nonEmpty(applicant.current_address),
      date_moved_into_current_address: nonEmpty(applicant.date_moved_into_current_address),
      previous_addresses: [...checkPreviousAddresses(applicant.previous_addresses).slice(0, -1), {
        ...checkPreviousAddresses(applicant.previous_addresses).slice(-1)[0],
        date_moved_into_address: checkLastAddress(applicant.previous_addresses)
      }],
      residential_status: nonEmpty(applicant.residential_status),
      employment_history: _.reduce(applicant.employment_history, (total, current, index, all) => {
        const {
          employer_name,
          start_date,
          end_date,
          status,
          job_title,
          gross_annual_salary,
          bonus,
          commission,
          address,
          contact_details
        } = current;
        const lastItemErrors = index !== all.length - 1 ? {} : {
          start_date: _.every(all, item => earlierThan(item.start_date, [3, 'years'])) ?
            'The earliest has to be at least 2 years ago' : nonEmpty(start_date)
        };

        return [...total, {
          employer_name: nonEmpty(employer_name),
          start_date: nonEmpty(start_date),
          end_date: nonEmpty(end_date),
          status: nonEmpty(status),
          job_title: nonEmpty(job_title),
          gross_annual_salary: nonEmpty(gross_annual_salary),
          bonus: nonEmpty(bonus),
          commission: nonEmpty(commission),
          address: nonEmpty(address),
          contact_details: nonEmpty(contact_details),
          ...lastItemErrors
        }];
      }, [])
    }))
  };

  return errors;
};

const renderUsersSelect = (field) => (
  <div>
    <p>{field.label}</p>
    <UsersSelect
      {...field.input}
      />
  </div>
)

const renderStatusSelect = (field) => (
  <div>
    <p>{field.label}</p>
    <StatusSelect
      {...field.input}
      />
  </div>
)

const renderPreviousAddresses = ({ fields }) => (
  <div>
    <h3>Previous Addresses</h3>
    {fields.map((field, index) => (
      <div
        key={index}
        className="previous-address"
      >
        <div className="pull-right">
          <Button
            bsSize="xsmall"
            bsStyle="danger"
            onClick={() => fields.remove(index)}
          >
            <i className="fa fa-remove" />
          </Button>
        </div>
        <Field
          name={`${field}.address`}
          component={Input}
          type="textarea"
          label="Previous Address"
        />
        <Field
          name={`${field}.date_moved_into_address`}
          component={Input}
          type="date"
          label="Date moved into previous address"
        />
      </div>
    ))}
    <div>
      <Button onClick={() => fields.push({})}>Add previous address</Button>
    </div>
  </div>
);

const renderEmploymentHistory = ({ fields }) => (
  <div>
    <h3>Previous Employments</h3>
    {fields.map((field, index) => (
      <div
        key={index}
        className="previous-workplace"
      >
        <div className="pull-right">
          <Button
            bsSize="xsmall"
            bsStyle="danger"
            onClick={() => fields.remove(index)}
          >
            <i className="fa fa-remove" />
          </Button>
        </div>
        <Field
          name={`${field}.employer_name`}
          component={Input}
          type="text"
          label="Current employer's name"
        />
        <Field
          name={`${field}.start_date`}
          component={Input}
          type="date"
          label="Start date"
        />
        <Field
          name={`${field}.end_date`}
          component={Input}
          type="date"
          label="End date"
        />
        <Field
          name={`${field}.status`}
          component={Input}
          type="select"
          options={[
            'Full time',
            'Part time',
            'Self employed as company',
            'Self employed as sole trader',
            'Self employed as joint',
            'Contract',
            'Casual',
            'Home Duties',
            'Unemployed',
            'Retired'
          ]}
          label="Status"
        />
        <Field
          name={`${field}.job_title`}
          component={Input}
          type="text"
          label="Occupation/job title"
        />
        <Field
          name={`${field}.gross_annual_salary`}
          component={Input}
          type="number"
          min={0}
          label="Gross Annual Salary"
        />
        <Field
          name={`${field}.bonus`}
          component={Input}
          type="number"
          min={0}
          label="Bonus"
        />
        <Field
          name={`${field}.commission`}
          component={Input}
          type="number"
          min={0}
          label="Commission"
        />
        <Field
          name={`${field}.address`}
          component={Input}
          type="textarea"
          label="Employer address"
        />
        <Field
          name={`${field}.contact_details`}
          component={Input}
          type="textarea"
          label="Contact details for employer (name and number)"
        />
      </div>
    ))}
    <div>
      <Button onClick={() => fields.push({})}>Add employment</Button>
    </div>
  </div>
);

const renderApplicants = ({ fields }) => (
  <div>
    <h2>Applicants</h2>
    <div>
      <Button onClick={() => fields.push({})}>Add new applicant</Button>
    </div>
    <div className="applicants-forms">
      {fields.map((app, index) => (
        <div className="applicant" key={index}>
          <h3>
            Personal Details
            <div className="pull-right">
              <Button
                bsSize="xsmall"
                bsStyle="danger"
                onClick={() => fields.remove(index)}
              >
                <i className="fa fa-remove" />
              </Button>
            </div>
          </h3>
          <Field
            name={`${app}.title`}
            component={Input}
            type="datalist"
            options={[
              'Mr',
              'Mrs',
              'Miss',
              'Mx',
              'Dr'
            ]}
            label="Title"
          />
          <Field
            name={`${app}.first_name`}
            component={Input}
            type="text"
            label="First Name"
          />
          <Field
            name={`${app}.middle_name`}
            component={Input}
            type="text"
            label="Middle Name"
          />
          <Field
            name={`${app}.surname`}
            component={Input}
            type="text"
            label="Surname"
          />
          <Field
            name={`${app}.date_of_birth`}
            component={Input}
            type="date"
            label="Date of birth"
          />
          <Field
            name={`${app}.mother_maiden_name`}
            component={Input}
            type="text"
            label="Mother's Maiden Name"
          />
          <Field
            name={`${app}.marital_status`}
            component={Input}
            type="select"
            options={[
              'Single',
              'Married',
              'De-facto',
              'Separated',
              'Divorced',
              'Widowed'
            ]}
            label="Marital status"
          />
          <Field
            name={`${app}.number_of_dependants`}
            component={Input}
            type="number"
            min={0}
            label="Number of dependants"
          />
          <Field
            name={`${app}.ages_of_dependants`}
            component={Input}
            type="text"
            label="Ages of dependants"
          />
          <Field
            name={`${app}.phone_home`}
            component={Input}
            type="tel"
            label="Home phone number"
          />
          <Field
            name={`${app}.phone_work`}
            component={Input}
            type="tel"
            label="Work phone number"
          />
          <Field
            name={`${app}.phone_mobile`}
            component={Input}
            type="tel"
            label="Mobile phone number"
          />
          <Field
            name={`${app}.email_address`}
            component={Input}
            type="email"
            label="Email address"
          />
          <Col sm={9} smOffset={3}>
            <Field
              name={`${app}.first_home_owner`}
              component={Input}
              label="First Home Owner"
              type="checkbox"
            />
            <Field
              name={`${app}.permanent_resident`}
              component={Input}
              label="Permanent resident in Australia"
              type="checkbox"
            />
            <Field
              name={`${app}.dependant_spouse`}
              component={Input}
              label="Dependant Spouse"
              type="checkbox"
            />
          </Col>
          <h3>Address Details</h3>

          <div>
            <Field
              name={`${app}.current_address`}
              component={Input}
              type="textarea"
              label="Current Address"
            />
            {index !== 0 && (
              <CopyValue
                form="application-form"
                from={`applicants[0].current_address`}
                to={`${app}.current_address`}
                title="copy address from first applicant"
              />
            )}
          </div>

          <Field
            name={`${app}.date_moved_into_current_address`}
            component={Input}
            type="date"
            label="Date moved in"
          />
          <Field
            name={`${app}.residential_status`}
            component={Input}
            type="select"
            options={[
              'Own home',
              'Renting',
              'Living with family member',
              'Boarding'
            ]}
            label="Residential Status"
          />
          <Field
            name={`${app}.postal_address`}
            component={Input}
            type="textarea"
            label="Postal address (if different)"
          />
          <FieldArray
            name={`${app}.previous_addresses`}
            component={renderPreviousAddresses}
          />
          <h3>Income Details</h3>
          <FieldArray
            name={`${app}.employment_history`}
            component={renderEmploymentHistory}
          />
          <Field
            name={`${app}.family_tax`}
            component={Input}
            type="number"
            label="Family tax A&B"
          />
          <Field
            name={`${app}.other_income`}
            component={Input}
            type="number"
            label="Other income"
          />
        </div>
      ))}
    </div>
    <h3>Client Objectives</h3>
    <Row>
      <Col sm={4}>
        <Field
          name="objective_better_rate"
          component={Input}
          label="Better Rate"
          type="checkbox"
        />
        <Field
          name="objective_end_of_fixed_rate"
          component={Input}
          label="End of fixed rate"
          type="checkbox"
        />
        <Field
          name="objective_debt_consolidation"
          component={Input}
          label="Debt consolidation"
          type="checkbox"
        />
        <Field
          name="objective_additional_funds"
          component={Input}
          label="Additional Funds"
          type="checkbox"
        />
        <Field
          name="objective_construction"
          component={Input}
          label="Construction"
          type="checkbox"
        />
        <Field
          name="objective_lock_in_equity"
          component={Input}
          label="Lock in Equity"
          type="checkbox"
        />
        <Field
          name="objective_dislikes_lender"
          component={Input}
          label="Dislikes lender"
          type="checkbox"
        />
      </Col>
      <Col sm={4}>
        <Field
          name="objective_change_structure"
          component={Input}
          label="Change Structure"
          type="checkbox"
        />
        <Field
          name="objective_income_rate"
          component={Input}
          label="Rate"
          type="checkbox"
        />
        <Field
          name="objective_income_speed"
          component={Input}
          label="Speed"
          type="checkbox"
        />
        <Field
          name="objective_valuation"
          component={Input}
          label="Valuation"
          type="checkbox"
        />
        <Field
          name="objective_property_type"
          component={Input}
          label="Property type"
          type="checkbox"
        />
        <Field
          name="objective_construction_purchase"
          component={Input}
          label="Construction"
          type="checkbox"
        />
        <Field
          name="objective_preapproval"
          component={Input}
          label="Preapproval"
          type="checkbox"
        />
      </Col>
      <Col sm={4}>
        <Field
          name="objective_income_increase"
          component={Input}
          label="Income increase"
          type="checkbox"
        />
        <Field
          name="objective_extra_earner"
          component={Input}
          label="Extra earner"
          type="checkbox"
        />
        <Field
          name="objective_add_inc_source"
          component={Input}
          label="Add inc source"
          type="checkbox"
        />
        <Field
          name="objective_income_reduce"
          component={Input}
          label="Income reduce"
          type="checkbox"
        />
        <Field
          name="objective_school_fees"
          component={Input}
          label="School fees"
          type="checkbox"
        />
      </Col>
    </Row>
  </div>
);

const renderAssets = ({ applicants, title, fields }) => {
  if (!applicants) {
    return null;
  }

  return (
    <div>
      <h2>
        {title}
        <div className="pull-right">
          <Button onClick={() => fields.push({})}>Add new asset</Button>
        </div>
      </h2>

      <Table striped bordered condensed hover>
        <thead>
          <tr>
            <th />
            <th>Asset Type</th>
            <th>Details</th>
            <th>Estimated Value</th>
            <th>Investment Income (yearly)</th>
            {applicants.map((app, index) => (
              <th key={`asset-applicant-${index}`}>App {index + 1}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {fields.map((asset, index) => (
            <tr key={`asset-${index}`}>
              <td>
                <Button
                  bsSize="xsmall"
                  bsStyle="danger"
                  onClick={() => fields.remove(index)}
                >
                  <i className="fa fa-remove" />
                </Button>
              </td>
              <td>
                <Field
                  name={`${asset}.type`}
                  component={Input}
                  type="select"
                  options={[
                    'Owner Occupied',
                    'Investment Property'
                  ]}
                />
              </td>
              <td>
                <Field
                  name={`${asset}.details`}
                  component={Input}
                  type="textarea"
                />
              </td>
              <td>
                <Field
                  name={`${asset}.estimated_value`}
                  component={Input}
                  type="number"
                  min={0}
                />
              </td>
              <td>
                <Field
                  name={`${asset}.rental_income`}
                  component={Input}
                  type="number"
                  min={0}
                />
              </td>
              {applicants.map((app, i) => (
                <th key={`asset-applicant-value-${i}`}>
                  <Field
                    name={`${asset}.owners[${i}]`}
                    component={Input}
                    type="checkbox"
                  />
                </th>
              ))}
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

const renderLiabilities = ({ applicants, fields }) => {
  if (!applicants) {
    return null;
  }

  return (
    <div>
      <h2>
        Liabilities
        <div className="pull-right">
          <Button onClick={() => fields.push({})}>Add new liability</Button>
        </div>
      </h2>

      <Table striped bordered condensed hover>
        <thead>
          <tr>
            <th />
            <th>Type</th>
            <th>Lender</th>
            <th>Limit</th>
            <th>Balance</th>
            <th>Mthy Payment</th>
            {applicants.map((app, index) => (
              <th key={`liability-applicant-${index}`}>App {index + 1}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {fields.map((liability, index) => (
            <tr key={`liability-${index}`}>
              <td>
                <Button
                  bsSize="xsmall"
                  bsStyle="danger"
                  onClick={() => fields.remove(index)}
                >
                  <i className="fa fa-remove" />
                </Button>
              </td>
              <td>
                <Field
                  name={`${liability}.type`}
                  component={Input}
                  type="select"
                  options={[
                    'Mortgage',
                    'Credit Card',
                    'Car Loan',
                    'Personal Loan',
                    'Other'
                  ]}
                />
              </td>
              <td>
                <Field
                  name={`${liability}.lender`}
                  component={Input}
                  type="text"
                />
              </td>
              <td>
                <Field
                  name={`${liability}.limit`}
                  component={Input}
                  type="number"
                />
              </td>
              <td>
                <Field
                  name={`${liability}.balance`}
                  component={Input}
                  type="number"
                />
              </td>
              <td>
                <Field
                  name={`${liability}.monthlyPayment`}
                  component={Input}
                  type="number"
                  min={0}
                />
              </td>
              {applicants.map((app, i) => (<th key={`liability-applicant-${i}`}>
                <Field
                  name={`${liability}.parties[${i}]`}
                  component={Input}
                  type="checkbox"
                />
              </th>))}
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

const selector = formValueSelector('application-form');

@reduxForm({
  form: 'application-form',
  warn: validate,
})
@connect((state) => ({
  applicants: selector(state, 'applicants')
}), {
  arrayPush
})
export default class ApplicationForm extends Component {
  render() {
    const {
      applicants,
      handleSubmit
    } = this.props;
    const model = this.props.initialValues;

    if (window.URLSearchParams) {
      const urlParams = new URLSearchParams(window.location.search);
      const urlParamName = urlParams.get('name');
      if (urlParamName) {
        const nameParts = urlParamName.split(' ', 2);
        const firstName = nameParts[0];
        applicants[0].first_name = nameParts[0];
        applicants[0].surname = nameParts[1];
      }
    }

    return (
      <Form horizontal onSubmit={handleSubmit}>
        <Field
          name="name"
          component={Input}
          type="text"
          label="Name"
        />

        <PermittedAttribute type="application" model={model} attribute="users">
          <Field
            name="users"
            component={renderUsersSelect}
            label="Users"
          />
        </PermittedAttribute>

        <PermittedAttribute type="application" model={model} attribute="users">
          <Field
            name="status"
            component={renderStatusSelect}
            label="Status"
          />
        </PermittedAttribute>

        <FieldArray
          name="applicants"
          component={renderApplicants}
        />
        <Field
          name="additional_comments"
          component={Input}
          type="textarea"
          label="Additional Comments"
        />
        <FieldArray
          name="property_details"
          title="Property Details"
          component={renderAssets}
          applicants={applicants}
        />
        <FieldArray
          name="other_assets"
          title="Other Assets"
          component={renderAssets}
          applicants={applicants}
        />
        <FieldArray
          name="liabilities"
          component={renderLiabilities}
          applicants={applicants}
        />
      </Form>
    );
  }
}
