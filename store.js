import {
  applyMiddleware,
  combineReducers,
  createStore,
} from 'redux';
import reduxThunk from 'redux-thunk';
import { apiMiddleware } from 'redux-api-middleware';
import createSagaMiddleware from 'redux-saga';
import createLogger from 'redux-logger';
import { rootReducer } from 'ducks';
import { rootSaga } from 'sagas';

const logger = createLogger({
  predicate: (getState, action) => !action.type.startsWith('@@redux-form'),
  collapsed: true
});

const sagaMiddleware = createSagaMiddleware();

const middlewares = [
  reduxThunk,
  apiMiddleware,
  sagaMiddleware,
];

if (ENV === 'development') {
  middlewares.push(logger);
}

const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(...middlewares)
);
sagaMiddleware.run(rootSaga);

if(module.hot) {
  module.hot.accept('./ducks', () => {
    // const {
    //   rootReducer: nextReducer,
    //   rootEpic: nextEpic,
    // } = require('./ducks/index');
    // store.replaceReducer(nextReducer);
    // epicMiddleware.replaceEpic(rootEpic);
  });
}

export default store;
