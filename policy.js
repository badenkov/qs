import { includes, get } from 'lodash';

const ROLE_ADMIN = 'admin';
const ROLE_BROKER = 'broker';
const ROLE_CLIENT = 'client';

function checkRole(user, ...roles) {
  if (!user || !user.role) {
    return false;
  }

  return includes(roles, user.role)
}

function isUserEqual(user1, user2) {
  if (!user1 || !user1.id) { return false }
  if (!user2 || !user2.id) { return false }

  return user1.id === user2.id;
}

const policy = {
  users: {
    show: (currentUser) => {
      const a = checkRole(currentUser, ROLE_ADMIN, ROLE_BROKER);

      return checkRole(currentUser, ROLE_ADMIN, ROLE_BROKER);
    },
  },
  user: {
    show: (currentUser, user) => {
      if (currentUser.id == user.id) {
        return true;
      } else if (checkRole(currentUser, ROLE_ADMIN, ROLE_BROKER)) {
        return true;
      } else {
        return false;
      }
    },

    create: (currentUser, user) => {
      if (currentUser.id == user.id) {
        return true;
      } else if (checkRole(currentUser, ROLE_ADMIN, ROLE_BROKER)) {
        return true;
      } else {
        return false;
      }
    },

    update: (currentUser, user) => {
      if (currentUser.id == user.id) {
        return true;
      } else if (checkRole(currentUser, ROLE_ADMIN, ROLE_BROKER)) {
        return true;
      } else {
        return false;
      }
    },

    destroy: (currentUser, user) => {
      if (checkRole(currentUser, ROLE_ADMIN, ROLE_BROKER)) {
        return true;
      } else {
        return false;
      }
    },

    permittedAttributes: (currentUser, user) => {
      if (isUserEqual(currentUser, user)) {
        return ['first_name', 'last_name', 'bio', 'email', 'password'];
      } else if (checkRole(currentUser, ROLE_ADMIN)) {
        return ['first_name', 'last_name', 'bio', 'email', 'role'];
      } else if (checkRole(currentUser, ROLE_BROKER)) {
        return ['first_name', 'last_name', 'email', 'bio'];
      } else {
        return [];
      }
    }
  },
  applications: {
    show: (currentUser) => {
      const a = checkRole(currentUser, ROLE_ADMIN, ROLE_BROKER);

      return checkRole(currentUser, ROLE_ADMIN, ROLE_BROKER);
    },
  },
  application: {
    show: true,
    create: true,
    update: true,
    destroy: true,

    permittedAttributes: (currentUser, user) => {
      if (checkRole(currentUser, ROLE_ADMIN, ROLE_BROKER)) {
        return ['users', 'status'];
      } else {
        return [];
      }
    }
  },
  documents: {
    create: (currentUser, document) => {
      if (checkRole(currentUser, ROLE_ADMIN, ROLE_BROKER)) {
        return true;
      }
    },
  },
  document: {
    upload: true,
    destroy: (currentUser, document) => {
      return checkRole(currentUser, ROLE_ADMIN, ROLE_BROKER) || false;
    },
  }
}

// examples:
// can?(currentUser, 'application.create') will return true or false
// can?(currentUser, 'application.update', application) will return true or false
export function can(currentUser, type, model) {
  const rule = get(policy, type, false);

  if (typeof rule === 'function') {
    return rule(currentUser, model);
  } else {
    return rule;
  }
}

// examples:
// permittedAttributes(currentUser, 'user', user) will return ['first_name', 'last_name', 'email']
export function permittedAttributes(currentUser, type, model) {
  const rule = get(policy, `${type}.permittedAttributes`, []);

  if (typeof rule === 'function') {
    return rule(currentUser, model);
  } else {
    return rule;
  }
}
