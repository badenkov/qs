import axios from 'axios';

const BACKEND_URL = window.BACKEND_URL || '/';

function hasJwt() {
  return !!localStorage.getItem('jwt');
}

function getJwt() {
  return localStorage.getItem('jwt');
}

function getOptions() {
  const options = {
    headers: {},
  };
  if (hasJwt()) {
    options['headers']['Authorization'] = getJwt();
  }

  return options;
}

export function headers(headers = {}) {
  const result = {
    'Content-Type': 'application/json',
    ...headers,
  };
  if (hasJwt()) {
    result['Authorization'] = getJwt();
  }

  return result;
}

export function endpoint(path) {
  return `/api/${path}`;
}

export default {
  get: (path) => {
    return axios.get(BACKEND_URL + '/' + path, getOptions());
  },
  delete: (path) => {
    return axios.delete(BACKEND_URL + '/' + path, getOptions());
  },
  post: (path, data) => {
    return axios.post(BACKEND_URL + '/' + path, data, getOptions());
  },
}
