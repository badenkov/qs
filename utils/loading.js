export const LOADING = '__IS_LOADING__';

export function isLoading(value) {
  return value === LOADING;
}
