import { spawn } from 'redux-saga/effects'
import { checkAuth } from './checkAuth';
import { watchSignInWithEmailAndPassword } from './signInWithEmailAndPassword';
import { watchSignOut } from './signOut';
import { watchFetchAllUsers } from './users/fetchAll';

import { watchFetchApplications } from './applications/fetch';
import { watchCreateApplication } from './applications/create';
import { watchRemoveApplication } from './applications/remove';

import { watchFetchDocuments } from './documents/fetchDocuments';
import { watchCreateDocument } from './documents/createDocument';
import { watchUpdateDocument } from './documents/updateDocument';
import { watchUploadDocument } from './documents/uploadDocument';
import { watchDestroyDocument } from './documents/destroyDocument';

export function* rootSaga() {
  yield* [
    spawn(checkAuth),
    spawn(watchSignInWithEmailAndPassword),
    spawn(watchSignOut),
    spawn(watchFetchAllUsers),
    spawn(watchFetchApplications),
    spawn(watchCreateApplication),
    spawn(watchRemoveApplication),
    spawn(watchFetchDocuments),
    spawn(watchCreateDocument),
    spawn(watchUpdateDocument),
    spawn(watchUploadDocument),
    spawn(watchDestroyDocument),
  ];
}
