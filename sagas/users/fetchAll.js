import { takeEvery } from 'redux-saga';
import { put, call } from 'redux-saga/effects';
import api from '../../api';
import {
  FETCH_ALL,
  fetchUsersSuccess,
  fetchUesrsFailure,
} from '../../ducks/users';

export function* fetchUsers() {
  try {
    const { data: users } = yield call(api.get, '/api/users');
    yield put(fetchUsersSuccess(users));
  } catch (error) {
    yield put(fetchFailure(error));
  }
}

export function* watchFetchAllUsers() {
  yield* takeEvery(FETCH_ALL, fetchUsers);
}
