import { takeEvery } from 'redux-saga';
import { put, call } from 'redux-saga/effects';
import api from '../api';
import {
  SIGN_IN,
  signInSuccess,
  signInFailure,
} from '../ducks/auth';

export function* signInWithEmailAndPassword({ payload: { email, password } }) {
  try {
    const user = yield call(api.signInWithEmailAndPassword, email, password)
    yield put(signInSuccess(user));
  } catch (error) {
    yield put(signInFailure(error));
  }
}

export function* watchSignInWithEmailAndPassword() {
  yield* takeEvery(SIGN_IN, signInWithEmailAndPassword);
}
