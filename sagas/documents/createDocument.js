import { takeEvery } from 'redux-saga';
import { put, call } from 'redux-saga/effects';
import api from '../../api';
import {
  CREATE_DOCUMENT_REQUEST,
  createDocumentSuccess,
  createDocumentFailure,
} from '../../ducks/documents';

export function* watchCreateDocument() {
  yield* takeEvery(CREATE_DOCUMENT_REQUEST, createDocument);
}

export function* createDocument({ payload: { applicationID, document, onSuccess, onFailure } }) {
  try {
    const { data } = yield call(api.post, `/api/applications/${applicationID}/documents`, { document });
    console.log('createDocument', data);
    yield put(createDocumentSuccess(data));
    if (typeof onSuccess === 'function') {
      try {
        onSuccess(data);
      } catch (e) {}
    }

  } catch (error) {
    yield put(createDocumentFailure(error));
    if (typeof onFailure === 'function') {
      try {
        onFailure(error);
      } catch (e) {}
    }
  }
}
