import { takeEvery } from 'redux-saga';
import { put, call } from 'redux-saga/effects';
import api from '../../api';
import {
  DESTROY_DOCUMENT_REQUEST,
  destroyDocumentSuccess,
  destroyDocumentFailure,
} from '../../ducks/documents';

export function* watchDestroyDocument() {
  yield* takeEvery(DESTROY_DOCUMENT_REQUEST, destroyDocument);
}

export function* destroyDocument({ payload: { applicationID, id, onSuccess } }) {
  try {
    yield call(api.delete, `/api/applications/${applicationID}/documents/${id}`);
    yield put(destroyDocumentSuccess(id));
    if (typeof onSuccess === 'function') {
      try {
        onSuccess(id);
      } catch (e) {}
    }
  } catch (error) {
    yield put(destroyDocumentFailure(error));
  }
}
