import { takeEvery } from 'redux-saga';
import { put, call } from 'redux-saga/effects';
import api from '../../api';
import {
  FETCH_DOCUMENTS_REQUEST,
  fetchDocumentsSuccess,
  fetchDocumentsFailure,
} from '../../ducks/documents';

export function* watchFetchDocuments() {
  yield* takeEvery(FETCH_DOCUMENTS_REQUEST, fetchDocuments);
}

export function* fetchDocuments({ payload: { applicationID } }) {
  try {
    const { data: documents } = yield call(api.get, `/api/applications/${applicationID}/documents`);
    yield put(fetchDocumentsSuccess(documents));
  } catch (error) {
    yield put(fetchDocumentsFailure(error))
  }
}
