import { takeEvery } from 'redux-saga';
import { put, call } from 'redux-saga/effects';
import api from '../../api';
import {
  UPDATE_DOCUMENT_REQUEST,
  updateDocumentSuccess,
  updateDocumentFailure,
} from '../../ducks/documents';

export function* watchUpdateDocument() {
  yield* takeEvery(UPDATE_DOCUMENT_REQUEST, updateDocument);
}

export function* updateDocument({ payload: { applicationID, id, document, onSuccess } }) {
  try {
    const { data } = yield call(api.put, `/api/applications/${applicationID}/documents/${id}`, { document });
    yield put(updateDocumentSuccess(data));
    if (typeof onSuccess === 'function') {
      try {
        onSuccess(data);
      } catch(e) {}
    }

  } catch(error) {
    yield put(updateDocumentFailure(error));
  }
}
