import { takeEvery } from 'redux-saga';
import { put, call } from 'redux-saga/effects';
import api from '../../api';
import {
  UPLOAD_DOCUMENT_REQUEST,
  updateDocumentSuccess,
  updateDocumentFailure,
} from '../../ducks/documents';

export function* watchUploadDocument() {
  yield* takeEvery(UPLOAD_DOCUMENT_REQUEST, uploadDocument);
}

export function* uploadDocument({ payload: { applicationID, id, file, onSuccess } }) {
  try {
    const formData = new FormData();
    formData.append('file', file);
    const { data } = yield call(api.post, `/api/applications/${applicationID}/documents/${id}/upload`, formData);
    yield put(updateDocumentSuccess(data));
    if (typeof onSuccess === 'function') {
      try {
        onSuccess(data);
      } catch(e) {}
    }
  } catch(error) {
    yield put(updateDocumentFailure(error));
  }
}
