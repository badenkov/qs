import { takeEvery } from 'redux-saga';
import { put, call } from 'redux-saga/effects';
import {
  CHECK_AUTH_REQUEST,
  checkAuthSuccess,
  checkAuthFailure,
} from '../ducks/auth';
import api from '../api';

export function* checkAuth() {
  try {
    const isGuest = yield call(api.isGuest);

    if (isGuest) {
      yield put(checkAuthSuccess(null));
    } else {
      const res = yield call(api.get, '/api/users/me');
      yield put(checkAuthSuccess(res.data));
    }
  } catch (error) {
    yield put(checkAuthFailure(error));
  }
}
