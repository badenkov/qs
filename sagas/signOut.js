import { takeEvery } from 'redux-saga';
import { put, call } from 'redux-saga/effects';
import api from '../api';
import {
  SIGN_OUT,
  signOutSuccess,
} from '../ducks/auth';

export function* signOut() {
  yield call(api.signOut);
  yield put(signOutSuccess());
}

export function* watchSignOut() {
  yield* takeEvery(SIGN_OUT, signOut);
}
