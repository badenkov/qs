import { takeEvery } from 'redux-saga';
import { put, call } from 'redux-saga/effects';
import api from '../../api';
import {
  FETCH_REQUEST,
} from '../../ducks/applications';

export function* fetchApplications() {
  console.log('fetchApplications');
}

export function* watchFetchApplications() {
  yield* takeEvery(FETCH_REQUEST, fetchApplications);
}
