import { takeEvery } from 'redux-saga';
import { put, call } from 'redux-saga/effects';
import api from '../../api';
import {
  REMOVE_REQUEST,
  removeApplicationSuccess,
  removeApplicationFailure,
} from '../../ducks/applications';

export function* removeApplication({ payload: { id, onSuccess } }) {
  try {
    yield call(api.delete, `/api/applications/${id}`);
    yield put(removeApplicationSuccess());
    if (typeof onSuccess === 'function') {
      try {
        onSuccess();
      } catch(e) {}
    }
  } catch(error) {
    yield put(removeApplicationFailure(error))
  }
}

export function* watchRemoveApplication() {
  yield* takeEvery(REMOVE_REQUEST, removeApplication);
}
