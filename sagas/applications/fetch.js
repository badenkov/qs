import { takeEvery } from 'redux-saga';
import { put, call } from 'redux-saga/effects';
import api from '../../api';
import {
  FETCH_REQUEST,
  fetchApplicationsSuccess,
  fetchApplicationsFailure,
} from '../../ducks/applications';

export function* fetchApplications() {
  try {
    const { data: applications } = yield call(api.get, '/api/applications');
    yield put(fetchApplicationsSuccess(applications))
  } catch (error) {
    yield put(fetchApplicationsFailure(error));
  }
}

export function* watchFetchApplications() {
  yield* takeEvery(FETCH_REQUEST, fetchApplications);
}
