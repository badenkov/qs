import { takeEvery } from 'redux-saga';
import { put, call } from 'redux-saga/effects';
import api from '../../api';
import {
  CREATE_REQUEST,
  createApplicationSuccess,
  createApplicationFailure,
} from '../../ducks/applications';

export function* createApplication({ payload: { application, onSuccess }}) {
  try {
    const { data } = yield call(api.post, '/api/applications', { application });
    yield put(createApplicationSuccess(data.application));
    if (typeof onSuccess === 'function') {
      try {
        onSuccess(data.application);
      } catch (e) {}
    }
  } catch (error) {
    yield put(createApplicationFailure(error));
  }
}

export function* watchCreateApplication() {
  yield* takeEvery(CREATE_REQUEST, createApplication);
}
