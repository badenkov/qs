import React, { Component } from 'react';
import { Alert } from 'react-bootstrap';
import './styles.css';

export default class NotificationList extends Component {
  render() {
    return (
      <div className="notification-list">
        {this.props.notifications.map((n) => (
          <Alert key={n.key} bsStyle={n.style} onDismiss={n.onDismiss}>
            {n.title && <h4>{n.title}</h4>}
            {n.message}
          </Alert>
        ))}
      </div>
    );
  }
}
