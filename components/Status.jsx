import React from 'react';
import Steps from 'antd/lib/steps';
import 'antd/lib/steps/style/css';

const statuses = {
  documents_received: 0,
  documents_reviewed: 1,
  appointment_completed: 2,
  recommendations_send: 3,
  solution_agreed: 4,
  application_prepared: 5,
  application_signed_and_returned: 6,
}

export default ({ value }) =>
  <Steps size="small" current={statuses[value]}>
    <Steps.Step
      title="Documents"
      description="Received" />
    <Steps.Step
      title="Documents"
      description="Reviewed" />
    <Steps.Step
      title="Appointment"
      description="Completed"/>
    <Steps.Step
      title="Recommendations"
      description="Sent" />
    <Steps.Step
      title="Solution"
      description="Agreed" />
    <Steps.Step
      title="Application"
      description="Prepared" />
    <Steps.Step
      title="Application"
      description="Signed and Returned"/>
  </Steps>
