import React from 'react';
import 'react-select/dist/react-select.css';
import Select from 'react-select';

const options = [
  { value: 'documents_received', label: 'Documents Received' },
  { value: 'documents_reviewed', label: 'Documents Reviewed' },
  { value: 'appointment_completed', label: 'Appointment Completed'},
  { value: 'recommendations_send', label: 'Recommendations Sent' },
  { value: 'solution_agreed', label: 'Solution Agreed' },
  { value: 'application_prepared', label: 'Application Prepared' },
  { value: 'application_signed_and_returned', label: 'Application Signed and Returned' },
];

export default (props) =>
  <Select
    {...props}
    simpleValue={true}
    onBlur={() => null}
    options={options} />
