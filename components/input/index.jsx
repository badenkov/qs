import React from 'react';
import {
  Col,
  FormGroup,
  ControlLabel,
  FormControl,
  Checkbox
} from 'react-bootstrap';
import DatePicker from 'react-bootstrap-date-picker';
import './_datepicker.css';

export default (field) => {
  const {
    error,
    warning,
  } = field.meta;
  const validationState = (error || warning) ? 'warning' : null;

  if (field.type === 'select') {
    return (
      <FormGroup
        validationState={validationState}
      >
        {(field.label || field.input.label) && (
          <Col sm={3} componentClass={ControlLabel}>
            {field.label || field.input.label}
          </Col>
        )}
        <Col sm={(field.label || field.input.label) ? 9 : 12}>
          <FormControl
            componentClass="select"
            value={field.input.value}
            onChange={field.input.onChange}
          >
            <option>(choose one)</option>
            {field.options.map((value, index) => (
              <option key={index} value={value}>{value}</option>
            ))}
          </FormControl>
          {error && <span className="help-block text-danger">{error}</span>}
          {warning && <span className="help-block text-warning">{warning}</span>}
        </Col>
      </FormGroup>
    );
  } else if (field.type === 'checkbox') {
    return (
      <FormGroup
        validationState={validationState}
      >
        <Col sm={12}>
          <Checkbox
            checked={Boolean(field.input.value)}
            onChange={field.input.onChange}
          >
            {field.label}
          </Checkbox>
          {error && <span className="help-block text-danger">{error}</span>}
          {warning && <span className="help-block text-warning">{warning}</span>}
        </Col>
      </FormGroup>
    );
  } else if (field.type === 'date') {
    return (
      <FormGroup
        validationState={validationState}
      >
        {(field.label || field.input.label) && (
          <Col sm={3} componentClass={ControlLabel}>
            {field.label || field.input.label}
          </Col>
        )}
        <Col sm={(field.label || field.input.label) ? 9 : 12}>
          <DatePicker
            {...field.input}
            onChange={(value) => [
              field.onChange instanceof Function && field.onChange(value),
              field.input.onChange(value)
            ]}
            onBlur={() => null}
          />
          {error && <span className="help-block text-danger">{error}</span>}
          {warning && <span className="help-block text-warning">{warning}</span>}
        </Col>
      </FormGroup>
    );
  } else if (field.type === 'textarea') {
    return (
      <FormGroup
        validationState={validationState}
      >
        {field.label && <Col sm={3} componentClass={ControlLabel}>{field.label}</Col>}
        <Col sm={field.label ? 9 : 12}>
          <FormControl
            {...field.input}
            componentClass="textarea"
          />
          {error && <span className="help-block text-danger">{error}</span>}
          {warning && <span className="help-block text-warning">{warning}</span>}
        </Col>
      </FormGroup>
    );
  } else if (field.type === 'datalist') {
    return (
      <FormGroup
        validationState={validationState}
      >
        {field.label && <Col sm={3} componentClass={ControlLabel}>{field.label}</Col>}
        <Col sm={field.label ? 9 : 12}>
          <FormControl
            {...field.input}
            componentClass="input"
            list={field.name}
          />
          <datalist id={field.name}>
            {field.options.map((value, index) => (
              <option key={index} value={value}>{value}</option>
            ))}
          </datalist>
          {error && <span className="help-block text-danger">{error}</span>}
          {warning && <span className="help-block text-warning">{warning}</span>}
        </Col>
      </FormGroup>
    );
  } else if (field.type === 'number') {
    return (
      <FormGroup
        validationState={validationState}
      >
        {(field.label || field.input.label) && (
          <Col sm={3} componentClass={ControlLabel}>
            {field.label || field.input.label}
          </Col>
        )}
        <Col sm={(field.label || field.input.label) ? 9 : 12}>
          <FormControl
            {...field.input}
            min={field.min}
            type={field.type}
          />
          {error && <span className="help-block text-danger">{error}</span>}
          {warning && <span className="help-block text-warning">{warning}</span>}
        </Col>
      </FormGroup>
    );
  }

  return (
    <FormGroup
      validationState={validationState}
    >
      {(field.label || field.input.label) && (
        <Col sm={3} componentClass={ControlLabel}>
          {field.label || field.input.label}
        </Col>
      )}
      <Col sm={(field.label || field.input.label) ? 9 : 12}>
        <FormControl
          {...field.input}
          type={field.type}
        />
        {error && <span className="help-block text-danger">{error}</span>}
        {warning && <span className="help-block text-warning">{warning}</span>}
      </Col>
    </FormGroup>
  );
};
