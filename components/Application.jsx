import React, { Component } from 'react';
import styled from 'styled-components';
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router';
import Status from './Status';

const Application = styled.div`
  position: relative;
  margin-top: 10px;
  margin-bottom: 30px;
  padding-bottom: 10px;
  border-bottom: 1px solid #ccc;
`;

const Name = styled.h3`
  padding: 10px 0;
`;

const Actions = styled.div`
  margin-top: 10px;
  padding-top: 10px;
  border-top: 1px solid #ccc;
`;

export default ({ application, onDestroy }) => (
  <Application>
    <Name>{application.name}</Name>

    <Status value={application.status} />

    <Actions>
      <Link className="btn btn-primary btn-small" to={`/applications/edit/${application.id}`}>Edit</Link>
      &nbsp;
      <Link className="btn btn-success btn-small" to={`/applications/documents/${application.id}`}>Documents</Link>
      &nbsp;
      <div className="pull-right">
        <Button bsStyle="danger" bsSize="small" onClick={onDestroy}>Remove</Button>
      </div>
    </Actions>

  </Application>
);
