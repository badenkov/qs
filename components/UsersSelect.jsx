import React, { Component } from 'react';
import 'react-select/dist/react-select.css';
import Select from 'react-select';
import api from '../api';
import { map } from 'lodash';

const getFullname = (user) => {
  return `${user.first_name} ${user.last_name}`;
};

const getOptions = async (input) => {
  const { data } = await api.get('/api/users?role=client');

  return {
    options: map(data, (user) => ({ label: getFullname(user), value: user.id })),
    complete: true,
  };
};

export default (props) =>
  <Select.AsyncCreatable
    {...props}
    multi={true}
    joinValues={false}
    delimiter=","
    onBlur={() => null}
    loadOptions={getOptions} />
