import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signOut } from 'ducks/auth';
import isGuest from 'selectors/isGuest';
import { get } from 'lodash';
import {
  Navbar,
  Nav,
  NavItem,
  NavDropdown,
  MenuItem,
} from 'react-bootstrap';
import { Link } from 'react-router';
import Can from 'containers/Can';

@connect(
  (state) => ({
    currentUser: state.auth.currentUser,
  }),
  {
    signOut
  }
)
export default class MainMenu extends Component {
  render() {
    const {
      currentUser,
    } = this.props;

    return (
      <Navbar fluid>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/">Quick Select</Link>
          </Navbar.Brand>
        </Navbar.Header>

        <Nav>
          <Can type="applications.show">
            <Link to="/applications">
              {({ isActive, href, onClick }) => (
                <NavItem active={isActive} href={href} onClick={onClick} eventKey={2}>Applications</NavItem>
              )}
            </Link>
          </Can>

          <Can type="users.show">
            <Link to="/users">
              {({ isActive, href, onClick }) => (
                <NavItem active={isActive} href={href} onClick={onClick} eventKey={3}>Users</NavItem>
              )}
            </Link>
          </Can>

          {currentUser && currentUser.role === 'admin' && (
            <Link to="/sandbox">
              {({ isActive, href, onClick }) => (
                <NavItem active={isActive} href={href} onClick={onClick} eventKey={3}>Sandbox</NavItem>
              )}
            </Link>
          )}
        </Nav>

        {currentUser && (
          <Nav pullRight>
            <NavDropdown id="user-menu-dropdown" eventKey={1} title={`${get(currentUser, 'email')} ${get(currentUser, 'role')}`}>
              <MenuItem divider />
              <MenuItem eventKey={1.1} onClick={this.props.signOut}>Sign out</MenuItem>
            </NavDropdown>
          </Nav>
        )}

        {!currentUser && (
          <Nav pullRight>
            <Link to="/sign-in">
              {({ isActive, href, onClick }) => (
                <NavItem active={isActive} href={href} onClick={onClick} eventKey={1}>Sign in</NavItem>
              )}
            </Link>
          </Nav>
        )}

      </Navbar>
    )
  }
}
