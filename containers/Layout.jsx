import React, { Component } from 'react';
import { connect } from 'react-redux';
import { includes } from 'lodash';
import { Link } from 'react-router';
import {
  Grid,
  Row,
  Col,
} from 'react-bootstrap';
import NotificationList from 'components/notification-list';
import MainMenu from 'containers/MainMenu';
import { signOut } from 'ducks/auth';

@connect(
  ({
    notifications,
    auth: { initialized },
  }) => ({
    notifications,
  }),
)
export default class Layout extends Component {
  render() {
    return (
      <Grid fluid={true}>
        <MainMenu />
        {this.props.children}

        <NotificationList notifications={this.props.notifications} />
      </Grid>
    );
  }
}
