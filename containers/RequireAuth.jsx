import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Match, Redirect } from 'react-router';

@connect(({ auth: { currentUser }}) => ({ currentUser }))
export default class RequireAuth extends Component {
  render() {
    if (!this.props.currentUser) {
      return <Redirect to="/sign-in" />;
    }

    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}
