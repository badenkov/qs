import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { isLoading } from 'utils/loading';
import {
  fetchDocuments,
  updateDocument,
  uploadDocument,
  destroyDocument,
} from 'ducks/documents';

import {
  Button,
  ButtonGroup,
  Table,
} from 'react-bootstrap';
import Dropzone from 'react-dropzone';
import Can from 'containers/Can';

class DocumentFile extends Component {
  state = {
    uploading: false,
  }

  render() {
    const {
      applicationID,
      document,
      uploadDocument,
      fetchDocuments
    } = this.props;

    if (this.state.uploading) {
      return (
        <div>
          <i className="fa fa-spinner fa-spin fa-3x" />
        </div>
      );
    } else if (!document.file) {
      return (
        <Dropzone multiple={false} onDrop={(files) => {
          this.setState({ uploading: true });
          uploadDocument(applicationID, document.id, files[0], () => {
            // fetchDocuments(applicationID);
            this.setState({ uploading: false });
          });
        }}>
          <div style={{ padding: 10 }}>Drop file here, or click to select files to upload.</div>
        </Dropzone>
      );
    } else {
      return (
        <a href={document.file}>download</a>
      );
    };
  }

}


@connect(
  ({ documents: { items }}) => ({
    items,
  }),
  {
    fetchDocuments,
    updateDocument,
    uploadDocument,
    destroyDocument,
  }
)
export default class List extends Component {
  componentDidMount() {
    this.props.fetchDocuments(this.props.applicationID);
  }

  render() {
    if (isLoading(this.props.items)) {
      return (
        <div>
          <i className="fa fa-spinner fa-spin fa-3x" />
        </div>
      );
    }

    return (
      <Table striped bordered condensed hover>
        <thead>
          <tr>
            <th>Document Type</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {this.props.items.map((doc, index) => (
            <tr key={`doc-${doc.id}`}>
              <td>{doc.name}</td>
              <td>
                <Can type="document.upload" model={doc}>
                  <DocumentFile
                    fetchDocuments={this.props.fetchDocuments}
                    uploadDocument={this.props.uploadDocument}
                    applicationID={this.props.applicationID}
                    document={doc} />
                </Can>
              </td>
              <td>
                <Can type="document.destroy" model={doc}>
                  <Button
                    bsSize="xsmall"
                    bsStyle="danger"
                    onClick={() => {
                      if (confirm('Are you sure?')) {
                        this.props.destroyDocument(this.props.applicationID, doc.id);
                      }
                    }}>Remove</Button>
                </Can>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }
}

List.propTypes = {
  applicationID: PropTypes.number.isRequired,
}
