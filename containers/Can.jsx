import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { can } from '../policy';

@connect(({ auth: { currentUser } }) => ({ currentUser }))
export default class Can extends Component {
  render() {
    const {
      currentUser,
      children,
      type,
      model,
    } = this.props;

    if (can(currentUser, type, model)) {
      return children;
    } else {
      return null;
    }
  }
}

Can.propTypes = {
  children: PropTypes.element.isRequired,
  type: PropTypes.string.isRequired,
  model: PropTypes.object,
}
