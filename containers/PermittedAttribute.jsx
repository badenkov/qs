import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { permittedAttributes } from '../policy';
import { includes } from 'lodash';

@connect(({ auth: { currentUser } }) => ({ currentUser }))
export default class PermittedAttribute extends Component {
  render() {
    const {
      currentUser,
      children,
      type,
      model,
      attribute,
    } = this.props;

    const attrs = permittedAttributes(currentUser, type, model);

    if (includes(attrs, attribute)) {
      return children;
    } else {
      return null;
    }
  }
}

PermittedAttribute.propTypes = {
  children: PropTypes.element.isRequired,
  type: PropTypes.string.isRequired,
  attribute: PropTypes.string.isRequired,
  model: PropTypes.object,
}
