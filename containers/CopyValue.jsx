import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { formValueSelector, change } from 'redux-form';
import autobind from 'class-autobind';

@connect(
  (state, props) => {
    const selector = formValueSelector(props.form);
    return {
      from: selector(state, props.from),
    }
  },
  { change }
)
export default class CopyValue extends Component {
  propTypes = {
    form: PropTypes.string.isRequired,
    from: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }

  constructor(props) {
    super(props);
    autobind(this);
  }

  handleClick(e) {
    e.preventDefault();
    e.stopPropagation();

    const {
      index,
      from,
      to,
    } = this.props;

    this.props.change('application-form', to, from);
  }

  render() {
    if (!this.props.from) { return null; }

    return (
      <a href="#" onClick={this.handleClick}>{this.props.title}</a>
    );
  }
}
